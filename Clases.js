//CLASES
class Articulo {
    constructor(nombre, precio, cantidad, talla) {
        this.nombre = nombre;
        this.talla = talla;
        this.precio = precio;
        this.cantidad = cantidad;
    }
    get clave() {
        return this.nombre + this.talla;
    }

    get precioTotal() {
        return this.precio * this.cantidad
    }
}


// class CarroCompra {
//     #articulos
//     constructor() {
//         this.#articulos = new Map();
//     }

//     addArticulo(articulo) {
//         if (this.#articulos.has(articulo.clave)) {
//             let art = this.#articulos.get(articulo.clave);
//             art.cantidad = parseInt(art.cantidad) + parseInt(articulo.cantidad);
//         } else {
//             this.#articulos.set(articulo.clave, articulo);
//         }
//     }

//     getArticulos() {
//         console.log(this.#articulos.values());
//         return Array.from(this.#articulos.values());
//     }

//     setArticulos(compra) {
//         console.log(compra);
//         for (let aC of compra) {
//             let art = new Articulo(aC.nombre, aC.precio, aC.cantidad, aC.talla)
//             this.addArticulo(art);
//         }
//     }
// }

class CarroCompra {
    constructor() {
        this.articulos = [];
    }

    addArticulo(articulo) {
        this.articulos.push(articulo);
    }

    getArticulos() {
        return this.articulos;
    }

    setArticulos(articulos) {
        this.articulos = articulos;
    }
}