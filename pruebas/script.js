// Array para almacenar los productos agregados al carro
let carroProductos = [];

// Función para agregar un producto al carro
function agregarAlCarro(producto) {
  carroProductos.push(producto);
  actualizarCarro();
}

// Función para actualizar la lista del carro en el HTML
function actualizarCarro() {
  const carroLista = document.getElementById('carro-lista');
  carroLista.innerHTML = '';

  carroProductos.forEach((producto) => {
    const li = document.createElement('li');
    li.textContent = producto;
    carroLista.appendChild(li);
  });
}
