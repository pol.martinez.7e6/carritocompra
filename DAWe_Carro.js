let productos = [{
    modelo: "M1",
    nombre: "NINETIES DUO",
    foto: "NINETIES.png",
    descripcion: "Alternative ending to Pulp Fiction: Vincent and Jules jump in a time machine and land up on a wall in London in 2002 where Banksy’s given them bananas for guns. Let’s try that again, this time they end up in an 80’s 8-bit arcade game. And they say time travel’s not real. It’ll mess with you though. ",
    precio: 23.95,
    tallas: "XS,S,M,L,XXL",
}, {
    modelo: "M2",
    nombre: "VITRUVIAN",
    foto: "VITRUVIAN.png",
    descripcion: "Art History Man is a riot! Just when you think he’s expressionist, he shows his pop side. He’s Magritte meets Munch, Pollock meets Picasso, Miró meets Mondrian. He’s got it all; a true renaissance man. ",
    precio: 21.95,
    tallas: "S,M,L,XL",
}, {
    modelo: "M3",
    nombre: "MACACARENA",
    foto: "MACACARENA.png",
    descripcion: "He was about two decades too late but his friends didn’t have the heart to tell him that the dance craze was well and truly over. It was all about lip sync now. But he finally had something to do with his arms when he was dancing, and he just loved that naughty little bit where you swing your hips.",
    precio: 19.95,
    tallas: "L,XL,XXL",
}, {
    modelo: "M4",
    nombre: "RESERVOIR",
    foto: "RESERVOIR.png",
    descripcion: "The aliens stuck together after they retired from the arcade. The 2-bit shimmy from side to side was getting tiresome; the relentless pew-pew, now they watch old action movies instead. Tarantino never gets old.",
    precio: 21.95,
    tallas: "S,L,XL",
}];

// Función para crear un elemento HTML con atributos y contenido
function createElementWithAttributes(tagName, attributes, content) {
    const element = document.createElement(tagName);
    if (attributes) {
        for (const key in attributes) {
            element.setAttribute(key, attributes[key]);
        }
    }
    if (content) {
        element.innerHTML = content;
    }
    return element;
}

// Función para crear el selector de tallas
function createSizeSelector(tallas) {
    const select = document.createElement("select");
    tallas.split(",").forEach((talla) => {
        const option = document.createElement("option");
        option.value = talla;
        option.text = talla;
        select.appendChild(option);
    });
    return select;
}

// Función para mostrar los productos en la página
function muestraProductos() {
    const productosDiv = document.getElementById("productos");
    productos.forEach((producto) => {
        const article = createElementWithAttributes("article");

        const titulo = createElementWithAttributes("h3", null, producto.nombre);
        article.appendChild(titulo);

        const imagen = createElementWithAttributes("img", {
            src: "src/" + producto.foto,
        });
        article.appendChild(imagen);

        const descripcion = createElementWithAttributes("p", null, producto.descripcion);
        article.appendChild(descripcion);

        const precio = createElementWithAttributes("p", { class: "precio" });
        const precioSpan = createElementWithAttributes("span", null, "€" + producto.precio.toFixed(2));
        precio.appendChild(precioSpan);
        article.appendChild(precio);

        const tallasSelect = createSizeSelector(producto.tallas);
        article.appendChild(tallasSelect);

        const cantidadInput = createElementWithAttributes("input", {
            type: "number",
            id: producto.modelo,
            step: "1",
            min: "0",
        });
        article.appendChild(cantidadInput);

        const boton = createElementWithAttributes("button", null, "Añadir al Carro");
        boton.addEventListener("click", () => {
            const cantidad = parseInt(cantidadInput.value);
            if (cantidad > 0) {
                const articulo = new Articulo(producto.nombre, producto.precio, cantidad, tallasSelect.value);
                carro.addArticulo(articulo);
                anadirProductos();
                cantidadInput.value = "0";
            }
        });
        article.appendChild(boton);

        productosDiv.appendChild(article);
    });
}

// Función para añadir los productos al ticket de compra
function anadirProductos() {
    const tableBody = document.getElementById("tablebody");
    vaciarCarro();

    const articulos = carro.getArticulos();
    let subtotal = 0;

    articulos.forEach((articulo) => {
        const row = document.createElement("tr");
        row.appendChild(createElementWithAttributes("td", null, articulo.nombre));
        row.appendChild(createElementWithAttributes("td", null, articulo.talla));
        row.appendChild(createElementWithAttributes("td", null, articulo.precio.toFixed(2)));
        row.appendChild(createElementWithAttributes("td", null, articulo.cantidad));
        row.appendChild(createElementWithAttributes("td", null, (articulo.precioTotal).toFixed(2)));
        tableBody.appendChild(row);

        subtotal += articulo.precioTotal;
    });

    document.getElementById("subtotal").innerText = subtotal.toFixed(2);
    document.getElementById("ticket").style.display = "block";
}

// Función para vaciar el ticket de compra
function vaciarCarro() {
    const tableBody = document.getElementById("tablebody");
    while (tableBody.firstChild) {
        tableBody.removeChild(tableBody.firstChild);
    }
}

// Función para recuperar el pedido almacenado
function recuperaPedido() {
    const pedido = localStorage.getItem("pedido");
    if (pedido) {
        const compra = JSON.parse(pedido);
        carro.setArticulos(compra);
        anadirProductos();
    }
}

// Función para guardar el pedido en la persistencia (localStorage)
function guardarPedido() {
    const articulos = carro.getArticulos();
    const compra = JSON.stringify(articulos);
    localStorage.setItem("pedido", compra);
}

// Función para borrar el pedido de la persistencia (localStorage)
function borrarPedido() {
    localStorage.removeItem("pedido");
    vaciarCarro();
    document.getElementById("ticket").style.display = "none";
}

// Crear instancia de CarroCompra
const carro = new CarroCompra();

// Inicializar la página
function init() {
    muestraProductos();
    recuperaPedido();
}

// Asignar eventos a los botones
document.getElementById("guardarPedido").addEventListener("click", guardarPedido);
document.getElementById("borrarPedido").addEventListener("click", borrarPedido);
